## Analisis de Clases Latentes ##

library(poLCA)
library(car)
library(psych)
library(cluster)
library(gridExtra)
library(ggplot2)
library(GGally)

# CLUSTERS #

# Jerárquico Ward
c4 = agnes(D[,6:11],metric = "euclidean", stand = TRUE,diss=FALSE,method='ward')
plot(c4,cex.main=0.9,main="Cluster método Ward, distancias euclideas",xlab="6 variables manifiestas", ylab="Distancias",sub="")
abline(h=16,col=2)
abline(h=25,col=3)

cluster.means3 = aggregate(D[,6:11],by=list(cutree(c4, k = 3)), mean)
cluster.means5 = aggregate(D[,6:11],by=list(cutree(c4, k = 5)), mean)

round(cluster.means3,1)
round(cluster.means5,1)

D$grupo.agnes3 = cutree(c4,3)
D$grupo.agnes5 = cutree(c4,5)

# ACL #
set.seed(1234)

f = cbind(E_R,I_R,CSA_R,CSF_R,ND_R,VP_R)~1

lc3 <- poLCA(f,D,nclass=3,graphs = TRUE)
pchisq(lc3$Chisq, lc3$resid.df, lower.tail = F)

lc4 <- poLCA(f,D,nclass=4,graphs = TRUE) 
pchisq(lc4$Chisq, lc4$resid.df, lower.tail = F) 


dim(lc4$predcell[,1:6])
pred = lc4$predcell
pred = pred[order(pred$observed, decreasing = TRUE),]

aposteriori.4 = data.frame(lc4$predclass) 
D$aposteriori.4 = aposteriori.4$lc4.predclass

pred$patron = paste(pred[,1],pred[,2],pred[,3],pred[,4],pred[,5], pred[,6],sep="-")
D$patron    = paste(D[,30],D[,31],D[,32],D[,33],D[,34], D[,35],sep="-")

pred = merge(pred, D[,c("patron","aposteriori.4")], by = "patron", all.x =TRUE)
pred = unique(pred)

#clustering vs poLCA
round(prop.table(table(D$grupo.agnes3,D$aposteriori.4),1)*100,1)

#### ANÁLISIS DE LAS CLASES LATENTES ####
lc4

scatterplotMatrix(~E+I+CSA+CSF+ND+VP | grupo.agnes3, reg.line=FALSE,
                  smooth=FALSE, spread=FALSE, span=0.5, ellipse=FALSE, levels = c(.5, .5),
                  id.n=0, diagonal= 'density', by.groups=FALSE, data=D,col = c("yellow","green","red"),
                  cex.lab = 1.2)


